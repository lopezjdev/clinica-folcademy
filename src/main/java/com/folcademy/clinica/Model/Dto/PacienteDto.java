package com.folcademy.clinica.Model.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PacienteDto {

    Integer id;

    String dni;

    String nombre;

    String apellido;

    String telefono;


}
