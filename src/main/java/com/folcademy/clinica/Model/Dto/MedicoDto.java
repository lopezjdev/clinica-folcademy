package com.folcademy.clinica.Model.Dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDto {
    Integer id;

    @NotNull
    String profesion;

    int consulta;
}
