package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Integer> {

    List<Paciente> findAllByApellido(String apellido);

}
