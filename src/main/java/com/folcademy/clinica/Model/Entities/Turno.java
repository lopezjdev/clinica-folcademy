package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.sql.Date;
import java.sql.Time;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno
{
    @Id
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer idturno;
    @Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;
    @Column(name = "hora", columnDefinition = "TIME")
    public LocalTime hora;
    @Column(name = "atendido", columnDefinition = "TINYINT")
    public Boolean atendido;
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer idmedico;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente", insertable = false, updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico", insertable = false, updatable = false)
    private Medico medico;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return idturno != null && Objects.equals(idturno, turno.idturno);
    }
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}