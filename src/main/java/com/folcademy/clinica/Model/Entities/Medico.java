package com.folcademy.clinica.Model.Entities;


import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor

public class Medico
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer idmedico;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre;
    @Column(name = "apellido", columnDefinition = "VARCHAR")
    public String apellido;
    @Column(name = "profesion", columnDefinition = "VARCHAR")
    public String profesion;
    @Column(name = "consulta", columnDefinition = "INT(10) UNSIGNED")
    public int consulta = 0;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return idmedico != null && Objects.equals(idmedico, medico.idmedico);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}