package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getIdmedico(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoDto());

    }

    public Medico dtoToEntity(MedicoDto dto) {
        Medico entity = new Medico();
        entity.setIdmedico(dto.getId());
        entity.setNombre("");
        entity.setApellido("");
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }
}


