package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dto.TurnoDto;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;
    private final TurnoMapper turnoMapper;

    public TurnoController(TurnoService turnoService, TurnoMapper turnoMapper) {
        this.turnoService = turnoService;
        this.turnoMapper = turnoMapper;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<TurnoDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.listarTurnos())
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<TurnoDto> listarUno(@PathVariable(name = "idTurno") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.listarUno(id))
                ;
    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity) {
        return ResponseEntity
                .ok(turnoService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idturno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idturno") Integer id,
                                           @RequestBody @Validated TurnoDto dto){
        return ResponseEntity.ok(
                turnoService.editar(id, dto)
        );
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idturno}/{fecha}")
    public ResponseEntity<Boolean> editarFecha(@PathVariable(name = "idturno") int id,
                                                @PathVariable(name = "fecha") LocalDate fecha) {
        return ResponseEntity.ok(
                turnoService.editarFecha(id, fecha)
        );
    }

    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idturno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name="idturno") Integer id){
        return ResponseEntity.ok(
                turnoService.eliminar(id)
        );
    }
}
