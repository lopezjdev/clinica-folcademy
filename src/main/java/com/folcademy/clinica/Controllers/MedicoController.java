package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<MedicoDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        medicoService.listarMedicos())
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity
                .ok()
                .body(medicoService.listarUno(id))
                ;
    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto entity) {
        return ResponseEntity.ok(medicoService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody @Validated MedicoDto dto) {
        return ResponseEntity.ok(
                medicoService.editar(id, dto));
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idMedico}/consulta/{consulta}")
    public ResponseEntity<Boolean> editarConsulta (@PathVariable(name = "idMedico") int id,
                                                   @PathVariable(name = "consulta") int consulta) {
        return ResponseEntity.ok(
                medicoService.editarConsulta(id, consulta)
        );
    }

    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(
                medicoService.eliminar(id)
        );
    }
}
