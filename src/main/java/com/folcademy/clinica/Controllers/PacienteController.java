package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;
    private final PacienteMapper pacienteMapper;

    public PacienteController(PacienteService pacienteService, PacienteMapper pacienteMapper) {
        this.pacienteService = pacienteService;
        this.pacienteMapper = pacienteMapper;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findAllPacientes())
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<PacienteDto> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id))
                ;
    }

    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity) {
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idpaciente}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "idpaciente") int id,
                                                    @RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(
                pacienteService.editar(id, dto)
        );
    }

    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("{idpaciente}/telefono/{telefono}")
    public ResponseEntity<Boolean> editarTelefono(@PathVariable(name = "idpaciente") int id,
                                                  @PathVariable(name = "telefono") String telefono) {
        return ResponseEntity.ok(
                pacienteService.editarTelefono(id, telefono)
        );
    }

    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idpaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idpaciente") int id){
        return ResponseEntity.ok(
                pacienteService.eliminar(id)
        );
    }
}
