package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;

    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }


    @Override
    public List<PacienteDto> findAllPacientes() {

        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());


    }

    @Override
    public PacienteDto findPacienteById(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Error. La ID solicitada no coincide con ningún paciente");
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    }

    public PacienteDto agregar(PacienteDto entity){
        entity.setId(null);
        if(entity.getDni() == null || entity.getDni() == "")
            throw new BadRequestException("Error. Ingrese el DNI del paciente correctamente.");
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }

    public PacienteDto editar(Integer idPaciente, PacienteDto dto){
        if (!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("Error. La ID solicitada no está asociada a ningún paciente");
        dto.setId(idPaciente);
        return pacienteMapper.entityToDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(
                                dto
                        )
                )
        );
    }

    public boolean editarTelefono(Integer idPaciente, String telefono){
        if (pacienteRepository.existsById(idPaciente)) {
            Paciente entity = pacienteRepository.findById(idPaciente).orElse(new Paciente());
            entity.setTelefono(telefono);
            pacienteRepository.save(entity);
            return true;
        }
        return false;
    }

    public boolean eliminar(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Error. La ID solicitada no se encuentra en nuestra base de datos.");
        pacienteRepository.deleteById(id);
        return true;
    }

}
