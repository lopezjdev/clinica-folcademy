package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.MedicoDto;

import java.util.List;

public interface IMedicoService {

    List<MedicoDto> listarMedicos();
    MedicoDto listarUno(Integer id);
    MedicoDto agregar(MedicoDto entity);
    MedicoDto editar(Integer idMedico, MedicoDto dto);
    boolean editarConsulta(Integer idMedico, Integer consulta);
    boolean eliminar(Integer id);

}
