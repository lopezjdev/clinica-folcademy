package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.PacienteDto;

import java.util.List;

public interface IPacienteService {

    List<PacienteDto> findAllPacientes();
    PacienteDto findPacienteById(Integer id);
    PacienteDto agregar(PacienteDto entity);
    PacienteDto editar(Integer idpaciente, PacienteDto dto);
    boolean editarTelefono(Integer idpaciente, String telefono);
    boolean eliminar(Integer id);


}
