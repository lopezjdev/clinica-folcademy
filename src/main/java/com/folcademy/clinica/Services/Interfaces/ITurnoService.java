package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;

import java.util.List;

public interface ITurnoService {

    List<TurnoDto> listarTurnos();
    TurnoDto listarUno(Integer id);
}
